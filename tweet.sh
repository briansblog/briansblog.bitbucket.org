#! /bin/bash

cd _site
latest_post_path="$(find 20* | tail -n1)"
latest_post_title="$(grep '    <title>' ${latest_post_path} | sed -e 's/<[^>]*>//g')"
latest_post_url="https://briansblog.bitbucket.io/${latest_post_path}"
tweet.py ${latest_post_title} ${latest_post_url}
open "https://twitter.com/brian_m_edwards"
cd ..
