#! /bin/bash

jekyll build --config '_config.yml,_bitbucket_config.yml'

bitbucket_url="https://briansblog.bitbucket.io"

cd _site
echo ${bitbucket_url} > README.md
git add -A
git commit -a -m"bitbucket site"
git push
latest_post_path="$(find 20* | tail -n1)"
open "https://bedwards.github.io/blog/${latest_post_path}"
open "${bitbucket_url}/${latest_post_path}"
cd ..
